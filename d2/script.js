//Create a function called messageToSelf() - display a message to your past self in our console.
// Invoke the function 10 times


function msgToSelf() {
	console.log("Don't text her back")
}

//While loop
let count = 10

while(count !==0 ) {
	msgToSelf();
	count--;
}

count = 1
while (count <= 5) {
	console.log(count + " test")
	count++;
}

// Do-while loop
// Do while loop is similar to the While loop. However, do while will allow us to run our loop at least once.
// while -> we check the condition first before running our codeblock
// do while -> it will do an instruction first before it will check the condition to run again


// create a do-while loop which will be able to show the numbers in the console from 1-20 order

let counter = 1;

do {
	console.log(counter)
	counter++;
} while (counter <= 20)

let fruits = [["apple", 1],["applezzz", 2],["meow", 3],["test", 4]]

for(let x = 0; x < fruits.length; x++) {
	console.log(fruits[x])

}

// Global/local scope

// Global sccoped variables variables that can be accessed inside a function 
// or anyhwre else in our script
let userName = "super_knight_1000";

function sample() {

	let heroName = "One punch Man"
	console.log(heroName)
	console.log(userName)
}

sample();

console.log(userName)
// console.log(heroName)

//Strings are similar to Arrays

let name = "Alexandro"


// mini activity

function test(string) {
	for (let x = 0; x < string.length; x++) {
		console.log(string[x])
	}
}

test("meow")