//JSON OBJECTS
// JSON stands for JavaScript Object Notation
// JSON is used for serializing different data type into bytes.
// Serialization is the process of converting data into a series of bytes. 
// for easier transmission/transfer of information
// byte =? binary digits (1 and 0) that is used to represent a character

/*
Syntax:
	{
		"propertyA": "valueA",
		"propertyB": "valueB",
		"propertyC": "valueC",
	}
	
*/

/*
	JS OBJECT

	{
		city: "QC",
		province: "Metro Manila",
		country: "Philippines"
	}


	JSON
	{
		"city": "QC",
		"province": "Metro Manila",
		"country": "Philippines"
	}

*/

/*
"cities": [
	{
		"city": "QC",
		"province": "Metro Manila",
		"country": "Philippines"
	},
	{
		"city": "QC",
		"province": "Metro Manila",
		"country": "Philippines"
	},
	{
		"city": "QC",
		"province": "Metro Manila",
		"country": "Philippines"
	}

]*/


// JSON METHODS
// The Json object contains methods for parsing and converting data into stringified JSON


/*
-Stringified JSON is a javascript object converted into a string to be used in other function of
Javascript application
*/

let batchesArr = [
	{
		batchName: "Batch X"
	},
	{
		batchName: "Batch Y"
	}
]

console.log("Result from stringify")


// The "stringify" method is used to convert JS Objects into JSON(string)
console.log(JSON.stringify(batchesArr))

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "philippines"
	}
})

console.log(data)

/*
let firstName = prompt("First Name: ")
let lastName = prompt("Last Name: ")
let age = prompt("Age: ")
let address = [prompt("City: "), prompt("Country: "), prompt("Zip Code: ")]


let userDetails = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(userDetails)
*/

// converting stringied JSON into js objects
// Information is commonly sent to applications in stringified JSON and then converted back into objects
//Upon receiving data, JSON text can be converted to a JS object with parse

let batchesJSON = `[
	{
		"batchname": "Batch X"
	},
	{
		"batchname": "Batch Y"
	}
]`

console.log("Result from parse method: ")
console.log(JSON.parse(batchesJSON))


let stringifiedObject = `
	{
		"name": "John",
		"age": 31,
		"address": {
			"city": "Manila",
			"country": "philippines"
		}
	}
`

console.log(JSON.parse(stringifiedObject))